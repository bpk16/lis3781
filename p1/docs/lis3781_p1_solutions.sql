
-- -----------------------------------------------------
-- Schema bpk16
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS bpk16;
CREATE SCHEMA IF NOT EXISTS bpk16;
SHOW WARNINGS;
USE bpk16;

-- -----------------------------------------------------
-- Table  bpk16 . person 
-- -----------------------------------------------------
DROP TABLE IF EXISTS person;
CREATE TABLE IF NOT EXISTS person
(
   per_id  SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
   per_ssn  BINARY(64) NULL,
   per_salt  BINARY(64) NULL,
   per_fname  VARCHAR(15) NOT NULL,
   per_lname  VARCHAR(30) NOT NULL,
   per_street  VARCHAR(30) NOT NULL,
   per_city  VARCHAR(30) NOT NULL,
   per_state  CHAR(2) NOT NULL,
   per_zip  CHAR(9) NOT NULL,
   per_email  VARCHAR(100) NOT NULL,
   per_dob  DATE NOT NULL,
   per_type  ENUM('a','c','j') NOT NULL,
   per_notes  VARCHAR(255) NULL,
  PRIMARY KEY (per_id),
  UNIQUE Index ux_per_ssn(per_ssn ASC)
)
ENGINE = InnoDB
Default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table  bpk16 . phone 
-- -----------------------------------------------------
DROP TABLE IF EXISTS phone;
CREATE TABLE IF NOT EXISTS phone  
(
   phn_id  SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
   per_id  SMALLINT UNSIGNED NOT NULL,
   phn_num  BIGINT UNSIGNED NOT NULL,
   phn_type  ENUM('h','c','w','f') NOT NULL comment 'home, cell, work, fax',
   phn_notes  VARCHAR(255) NULL,
  PRIMARY KEY (phn_id),

  INDEX idx_per_id (per_id ASC),

  CONSTRAINT  fk_phone_person 
    FOREIGN KEY (per_id)
    REFERENCES person  (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
Default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table  bpk16 . attorney 
-- -----------------------------------------------------
DROP TABLE IF EXISTS attorney;
CREATE TABLE IF NOT EXISTS attorney  
(
   per_id  SMALLINT UNSIGNED NOT NULL,
   aty_start_date  DATE NOT NULL,
   aty_end_date  DATE NULL DEFAULT NULL,
   aty_hourly_rate  DECIMAL(5,2) UNSIGNED NOT NULL,
   aty_years_in_practice  TINYINT NOT NULL,
   aty_notes  VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY ( per_id ),

Index idx_per_id(per_id ASC),

  CONSTRAINT  fk_attorney_person 
    FOREIGN KEY (per_id)
    REFERENCES person  (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
Default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table  bpk16 . bar 
-- -----------------------------------------------------
DROP TABLE IF EXISTS bar;
CREATE TABLE IF NOT EXISTS bar  
(
   bar_id  TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
   per_id  SMALLINT UNSIGNED NOT NULL,
   bar_name  VARCHAR(45) NOT NULL,
   bar_notes  VARCHAR(255) NULL,
  PRIMARY KEY (bar_id),

  INDEX  idx_per_id (per_id ASC),

  CONSTRAINT  fk_bar_attorney 
    FOREIGN KEY (per_id)
    REFERENCES attorney (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
Default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table  bpk16 . specialty 
-- -----------------------------------------------------
DROP TABLE IF EXISTS specialty;
CREATE TABLE IF NOT EXISTS specialty  
(
   spc_id  TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
   per_id  SMALLINT UNSIGNED NOT NULL,
   spc_type  VARCHAR(45) NOT NULL,
   spc_notes  VARCHAR(255) NULL,
  PRIMARY KEY (spc_id),

  INDEX  idx_per_id (per_id ASC),

  CONSTRAINT  fk_specialty_attorney
    FOREIGN KEY (per_id)
    REFERENCES attorney (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
Default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table  bpk16 . client 
-- -----------------------------------------------------
DROP TABLE IF EXISTS client;
CREATE TABLE IF NOT EXISTS client 
(
   per_id  SMALLINT UNSIGNED NOT NULL,
   cli_notes  VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (per_id),

INDEX idx_per_id(per_id ASC),

  CONSTRAINT  fk_client_person
    FOREIGN KEY (per_id)
    REFERENCES  bpk16 . person  (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
Default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table  bpk16 . court 
-- -----------------------------------------------------
DROP TABLE IF EXISTS court;
CREATE TABLE IF NOT EXISTS court  
(
   crt_id  TINYINT UNSIGNED NOT NULL AUTO_INCREMENT,
   crt_name  VARCHAR(45) NOT NULL,
   crt_street  VARCHAR(30) NOT NULL,
   crt_city  VARCHAR(30) NOT NULL,
   crt_state  CHAR(2) NOT NULL,
   crt_zip  CHAR(9) NOT NULL,
   crt_phone  BIGINT NOT NULL,
   crt_email  VARCHAR(100) NOT NULL,
   crt_url  VARCHAR(100) NOT NULL,
   crt_notes  VARCHAR(255) NULL,
  PRIMARY KEY ( crt_id )
)
ENGINE = InnoDB
Default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table  bpk16 . judge 
-- -----------------------------------------------------
DROP TABLE IF EXISTS judge;
CREATE TABLE IF NOT EXISTS judge  
(
   per_id  SMALLINT UNSIGNED NOT NULL,
   crt_id  TINYINT UNSIGNED NULL DEFAULT NULL,
   jud_salary  DECIMAL(8,2) NOT NULL,
   jud_years_in_practice  TINYINT UNSIGNED NOT NULL,
   jud_notes  VARCHAR(255) NULL DEFAULT NULL,
  PRIMARY KEY (per_id),

  INDEX  idx_per_id(per_id ASC),
  INDEX  idx_crt_id(crt_id ASC),

  CONSTRAINT  fk_judge_person 
    FOREIGN KEY (per_id)
    REFERENCES person (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,

  CONSTRAINT  fk_judge_court
    FOREIGN KEY (crt_id)
    REFERENCES court (crt_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
Default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table  bpk16 . case 
-- -----------------------------------------------------
DROP TABLE IF EXISTS `case`;
CREATE TABLE IF NOT EXISTS `case`  
(
   cse_id  SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
   per_id  SMALLINT UNSIGNED NOT NULL,
   cse_type  VARCHAR(45) NOT NULL,
   cse_description  TEXT NOT NULL,
   cse_start_date  DATE NOT NULL,
   cse_end_date  DATE NULL,
   cse_notes  VARCHAR(255) NULL,
  PRIMARY KEY (cse_id),

  INDEX  idx_per_id  (per_id ASC),

  CONSTRAINT  fk_court_case_judge
    FOREIGN KEY (per_id)
    REFERENCES  judge (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
Default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table  bpk16 . assignment 
-- -----------------------------------------------------
DROP TABLE IF EXISTS assignment;
CREATE TABLE IF NOT EXISTS assignment  
(
   asn_id  SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
   per_cid  SMALLINT UNSIGNED NOT NULL,
   per_aid  SMALLINT UNSIGNED NOT NULL,
   cse_id  SMALLINT UNSIGNED NOT NULL,
   asn_notes  VARCHAR(255) NULL,
  PRIMARY KEY ( asn_id ),

  INDEX  idx_per_cid (per_cid ASC),
  INDEX  idx_per_aid (per_aid ASC),
  INDEX  idx_cse_id (cse_id ASC),

  UNIQUE INDEX ux_per_cid_per_aid_cse_id (per_cid ASC, per_aid ASC, cse_id ASC),

  CONSTRAINT  fk_assignment_client
    FOREIGN KEY (per_cid)
    REFERENCES  client  (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,

  CONSTRAINT  fk_assignment_attorney
    FOREIGN KEY (per_aid)
    REFERENCES attorney (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,

  CONSTRAINT  fk_assignment_case
    FOREIGN KEY (cse_id)
    REFERENCES `case` (cse_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
Default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table  bpk16 . judge_hist 
-- -----------------------------------------------------
DROP TABLE IF EXISTS judge_hist;
CREATE TABLE IF NOT EXISTS judge_hist  
(
   jhs_id  SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
   per_id  SMALLINT UNSIGNED NOT NULL,
   jhs_crt_id  TINYINT NULL,
   jhs_date  TIMESTAMP NOT NULL default current_timestamp(),
   jhs_type  ENUM('i','u','d') NOT NULL default 'i',
   jhs_salary  DECIMAL(8,2) NOT NULL,
   jhs_notes  VARCHAR(255) NULL,
  PRIMARY KEY (jhs_id),

  INDEX  idx_per_id(per_id ASC),

  CONSTRAINT  fk_judge_hist_judge 
    FOREIGN KEY (per_id)
    REFERENCES judge  (per_id)
    ON DELETE NO ACTION
    ON UPDATE CASCADE
)
ENGINE = InnoDB
Default character set = utf8mb4
collate = utf8mb4_0900_ai_ci;

SHOW WARNINGS;












START TRANSACTION;

INSERT INTO person
(per_id, per_ssn, per_salt, per_fname, per_lname, per_street, per_city, per_state, per_zip, per_email, per_dob, per_type, per_notes)
VALUES
(NULL, NULL, NULL, 'Steve', 'Rogers', '123 main st', 'Tampa','FL',324402222, 'srogers@gmail.com','1903-10-10', 'c', NULL),
(NULL, NULL, NULL, 'Luka', 'Doncic', '234 main st', 'Orlando','FL',322321234, 'ld@gmail.com','1903-10-11', 'c', NULL),
(NULL, NULL, NULL, 'Jackson', 'Embid', '345 main st', 'Jupiter','FL',358691234, 'je@gmail.com','1903-10-12', 'c', NULL),
(NULL, NULL, NULL, 'John', 'Harden', '456 main st', 'Miami','FL',315851234, 'jh@gmail.com','1903-10-13', 'c', NULL),
(NULL, NULL, NULL, 'Jerry', 'DeRozan', '567 main st', 'Boca Raton','FL',365971234, 'jd@gmail.com','1903-10-14', 'c', NULL),
(NULL, NULL, NULL, 'Tom', 'Caruso', '678 main st', 'Jacksonville','FL',335621234, 'tc@gmail.com','1903-10-15', 'a', NULL),
(NULL, NULL, NULL, 'Dakota', 'James', '789 main st', 'Tallahassee','FL',367471234, 'dj@gmail.com','1903-10-16', 'a', NULL),
(NULL, NULL, NULL, 'Randy', 'Rose', '890 main st', 'Plantation','FL',395871234, 'rr@gmail.com','1903-10-17', 'c', NULL),
(NULL, NULL, NULL, 'Eric', 'Gibson', '901 main st', 'Bradenton','FL',333321234, 'eg@gmail.com','1903-10-18', 'a', NULL),
(NULL, NULL, NULL, 'Kyle', 'Butler', '12 main st', 'Fort Lauderdale','FL',355571234, 'kb@gmail.com','1903-10-19', 'c', NULL),
(NULL, NULL, NULL, 'Peter', 'Wade', '21 main st', 'Key West','FL',321411234, 'pw@gmail.com','1903-10-20', 'j', NULL),
(NULL, NULL, NULL, 'Mary', 'Robinson', '23 main st', 'Crawfordville','FL',358741234, 'mr@gmail.com','1903-10-21', 'j', NULL),
(NULL, NULL, NULL, 'Sandra', 'Hero', '32 main st', 'Jacksonville','FL',329851234, 'sh@gmail.com','1903-10-22', 'j', NULL),
(NULL, NULL, NULL, 'Jessie', 'Adebayo', '45 main st', 'Orlando','FL',336891234, 'ja@gmail.com','1903-10-24', 'c', NULL),
(NULL, NULL, NULL, 'Karen', 'Monk', '54 main st', 'Tampa','FL',333361234, 'km@gmail.com','1903-10-25', 'c', NULL);

COMMIT;


START TRANSACTION;

INSERT INTO phone
(phn_id, per_id, phn_num, phn_type, phn_notes)
VALUES
(NULL, 1, 8032288827, 'c',NULL),
(NULL, 2, 5489632356, 'h',NULL),
(NULL, 3, 2854765986, 'w',NULL),
(NULL, 4, 2014532165, 'f',NULL),
(NULL, 5, 3869548520, 'c',NULL),
(NULL, 6, 3365245585, 'w',NULL),
(NULL, 7, 3012145214, 'h',NULL),
(NULL, 8, 5286445478, 'c',NULL),
(NULL, 9, 6398522659, 'f',NULL),
(NULL, 10, 2088825569, 'w',NULL),
(NULL, 11, 3488885285, 'c',NULL),
(NULL, 12, 6932584529, 'c',NULL),
(NULL, 13, 2596358529, 'f',NULL),
(NULL, 14, 1457296358, 'f',NULL),
(NULL, 15, 8524782459, 'c',NULL);

COMMIT;


START TRANSACTION;

INSERT INTO client
(per_id, cli_notes)
VALUES
(1, NULL),
(2, NULL),
(3, NULL),
(4, NULL),
(5, NULL);

COMMIT;


START TRANSACTION;

INSERT INTO attorney
(per_id, aty_start_date, aty_end_date, aty_hourly_rate, aty_years_in_practice, aty_notes)
VALUES
(6, '2006-10-11', NULL, 85, 5, NULL),
(7, '2006-10-12', NULL, 130, 28, NULL),
(8, '2006-10-13', NULL, 70, 17, NULL),
(9, '2006-10-14', NULL, 78, 13, NULL),
(10, '2006-10-15', NULL, 60, 24, NULL);

COMMIT;

START TRANSACTION;

INSERT INTO specialty
(spc_id, per_id, spc_type, spc_notes)
VALUES
(NULL, 6, 'business', NULL),
(NULL, 7, 'traffic', NULL),
(NULL, 8, 'bankruptcy', NULL),
(NULL, 9, 'insurance', NULL),
(NULL, 10, 'hudicial', NULL),
(NULL, 6, 'environmental', NULL),
(NULL, 7, 'criminal', NULL),
(NULL, 8, 'real estate', NULL),
(NULL, 9, 'malpractice', NULL),
(NULL, 10, 'business', NULL);

COMMIT;

START TRANSACTION;

INSERT INTO bar
(bar_id, per_id, bar_name, bar_notes)
VALUES
(NULL, 6, 'Florida bar', NULL),
(NULL, 7, 'Georgia bar', NULL),
(NULL, 8, 'Alabama bar', NULL),
(NULL, 9, 'Nebraska bar', NULL),
(NULL, 10, 'Iowa bar', NULL),
(NULL, 6, 'Indiana bar', NULL),
(NULL, 7, 'North Carolina bar', NULL),
(NULL, 8, 'South Carolina bar', NULL),
(NULL, 9, 'Virginia bar', NULL),
(NULL, 10, 'California bar', NULL),
(NULL, 6, 'New York bar', NULL),
(NULL, 7, 'Illinois bar', NULL),
(NULL, 8, 'Washington bar', NULL),
(NULL, 9, 'Oregon bar', NULL),
(NULL, 10, 'Utah bar', NULL),
(NULL, 6, 'Colorado bar', NULL),
(NULL, 7, 'Alaska bar', NULL),
(NULL, 8, 'Texas bar', NULL),
(NULL, 9, 'Arizona bar', NULL),
(NULL, 10, 'New Mexico bar', NULL);

COMMIT;


START TRANSACTION;

INSERT INTO court
(crt_id, crt_name, crt_street, crt_city, crt_state, crt_zip, crt_phone, crt_email, crt_url, crt_notes)
VALUES
(NULL, 'leon county circuit court', '987 main st', 'Tallahassee', 'FL', 324402222, 4546557486, 'leoncourts@gmail.com','leoncourts.com', NULL),
(NULL, 'broward county circuit court', '876 main st', 'Miami', 'FL', 352461234, 2489621774, 'browardcourts@gmail.com','browardcourts.com', NULL),
(NULL, 'Duval county circuit court', '765 main st', 'Jacksonville', 'FL', 398751234, 1456523025, 'duvalcourts@gmail.com','duvalcourts.com', NULL),
(NULL, 'orange county circuit court', '654 main st', 'Orlando', 'FL', 396581234, 8520146302, 'orangecourts@gmail.com','orangecourts.com', NULL),
(NULL, 'polk county circuit court', '543 main st', 'Tampa', 'FL', 312311234, 7896542580, 'polkcourts@gmail.com','polkcourts.com', NULL);

COMMIT;


START TRANSACTION;

INSERT INTO judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
VALUES
(11, 5, 150000, 10, NULL),
(12, 4, 185000, 3, NULL),
(13, 4, 135000, 2, NULL),
(14, 3, 170000, 6, NULL),
(15, 1, 120000, 1, NULL);

COMMIT;


INSERT INTO judge_hist
(jhs_id, per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
VALUES
(NULL, 11, 3, '2009-10-10', 'i', 130000, NULL),
(NULL, 12, 2, '2009-10-11', 'i', 140000, NULL),
(NULL, 13, 5, '2009-10-12', 'i', 150000, NULL),
(NULL, 13, 4, '2009-10-13', 'i', 160000, NULL),
(NULL, 14, 4, '2009-10-14', 'i', 170000, NULL),
(NULL, 15, 1, '2009-10-15', 'i', 180000, NULL),
(NULL, 14, 5, '2009-10-16', 'i', 190000, NULL),
(NULL, 14, 4, '2009-10-17', 'i', 180000, NULL),
(NULL, 13, 3, '2009-10-18', 'i', 170000, NULL),
(NULL, 12, 2, '2009-10-19', 'i', 160000, NULL);

COMMIT;


INSERT INTO `case`
(cse_id, per_id, cse_type, cse_description, cse_start_date, cse_end_date, cse_notes)
VALUES
(NULL, 13, 'civil','client logo is being used by a rival company', '2009-10-10', NULL, NULL),
(NULL, 12, 'criminal','client accused of assault', '2009-10-11', NULL, NULL),
(NULL, 14, 'criminal','client accused of assault', '2009-10-12', NULL, NULL),
(NULL, 11, 'civil','client logo is being used by a rival company', '2009-10-13', NULL, NULL),
(NULL, 13, 'civil','client logo is being used by a rival company', '2009-10-14', NULL, NULL),
(NULL, 14, 'criminal','client accused of assault', '2009-10-15', NULL, NULL),
(NULL, 12, 'civil','client logo is being used by a rival company', '2009-10-16', NULL, NULL),
(NULL, 15, 'civil','client logo is being used by a rival company', '2009-10-17', NULL, NULL);
COMMIT;


START TRANSACTION;

INSERT INTO assignment
(asn_id, per_cid, per_aid, cse_id, asn_notes)
VALUES
(NULL, 1, 6, 7, NULL),
(NULL, 2, 6, 6, NULL),
(NULL, 3, 7, 2, NULL),
(NULL, 4, 8, 2, NULL),
(NULL, 5, 9, 5, NULL),
(NULL, 1, 10, 1, NULL),
(NULL, 2, 6, 3, NULL),
(NULL, 3, 7, 8, NULL),
(NULL, 4, 8, 8, NULL),
(NULL, 5, 9, 8, NULL),
(NULL, 4, 10, 4, NULL);

COMMIT;


DROP PROCEDURE IF EXISTS CreatePersonSSN;
DELIMITER $$
CREATE PROCEDURE CreatePersonSSN()
BEGIN

 DECLARE x, y INT;
 SET x = 1;

 select count(*) into y from person;

 WHILE x <= y DO
 
  SET @salt=RANDOM_BYTES(64);
  SET @ran_num=FLOOR(rAND()*(999999999-111111111+1))+111111111;
  SET @ssn=unhex(sha2(concat(@salt, @ran_num), 512));

  update person
  set per_ssn=@ssn, per_salt=@salt
  where per_id=x;

 SET x = x + 1;

 END WHILE;

END$$
DELIMITER ;
call CreatePersonSSN();

show warnings;


select * from person;
DO SLEEP(3);


select * from phone;
DO SLEEP(3);


select * from client;
DO SLEEP(3);


select * from attorney;
DO SLEEP(3);


select * from specialty;
DO SLEEP(3);


select * from bar;
do sleep(3);


select * from court;
do sleep(3);


select * from judge;
do sleep(3);


select * from judge_hist;
do sleep(3);


select * from `case`;
do sleep(3);


select * from assignment;
do sleep(3);

exit












