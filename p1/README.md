
# LIS3781 Advanced Database Management

## Brian Kelly

### Project #1 Requirements:

1. Screenshot of P1 ERD
2. Link to ERD MWB file
3. Link to SQL solutions file
4. Screenshot of person select statement

#### Assignment Links

|*P1 docs: lis3781_p1_model.mwb and lis3781_p1_solutions.sql*| 
| :---                       |
|[P2 MWB File](docs/lis3781_p1_model.mwb "P2 ERD in .mwb format")|
|[P2 SQL File](docs/lis3781_p1_solutions.sql "P2 SQL Script")|

#### Assignment Requirements:

|*Picture of P1 ERD*|
|:---               |
|[![P1 ERD](img/p1ERD.png)]|

|*Picture of Person select statement*|
|:---               |
|[![Person Select Statement](img/personSelectStatement.png)]|



