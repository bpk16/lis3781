> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 Advanced Database Management

## Brian Kelly

### LIS 3781 Requirements:

*Course Work Links (Assignments):*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Screenshot of MySQL Code
    - Screenshot of Populated Tables
3. [A3 README.md](a3/README.md "My A3 README.md file")
    -  Screenshot of my SQL Code (Part A-E)
    - Screenshot of my populated tables in Oracle
    - Screenshot of SQL code for required reports (optional)
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - 
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - 


*Course Work Links (Projects):*

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Screenshot of P1 ERD
    - Link to ERD MWB file
    - Link to SQL solutions file
    - Screenshot of Person select statement
2. [P2 README.md](p2/README.md "My P2 README.md file")
    - Screenshot of at least one mongoDB shell command(s)
    - At least one required report
    - Answer Chapter 16 questions



