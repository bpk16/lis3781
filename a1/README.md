

# LIS 3781

## Brian Kelly

### Assignment #1  Requirements:

*Completed Steps:*

1. Distributed Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relationship Diagram, and SQL code (optional)
5. Bitbucket repo links
     a) this assignment
     b) completed tutorial (bitbucketstationlocation)

#### README.md file should include the following items:

* Screenshot of A1 ERD
* Ex1. SQL Solution
* Git commands with short descriptions


> #### Git commands w/ short descriptions:

1. git-init - Create an empty Git repository or reinitialize an existing one
2. git-status - Show the working tree status
3. git-add - Add file contents to the index
4. git-commit - Record changes to the repository
5. git-push - Update remote refs along with associated objects
6. git-pull - Fetch from and integrate with another repository or a local branch
7. git-config - Get and set repository or global options

#### Assignment Screenshots:

| *Screenshot of A1 ERD Image* |
| :---                         |
|![A1 ERD Screenshot](img/a1ERD.PNG)|

| *Screenshot of AMPPS*|*Screenshot of A1 Ex1*|
| :---                 | :---                 |
|![AMPPS Installation Screenshot](img/ampps.png)|![A1 Exercise 1](img/a1exercise1.png)|

| *Screenshot of A1 Ex2*|*Screenshot of A1 Ex3*|
| :---                 | :---                 |
|![A1 Exercise 2](img/a1exercise2.png)|![A1 Exercise 3](img/a1exercise3.png)|

| *Screenshot of A1 Ex4*|*Screenshot of A1 Ex5*|
| :---                 | :---                 |
|![A1 Exercise 4](img/a1exercise4.png)|![A1 Exercise 5](img/a1exercise5.png)|

| *Screenshot of A1 Ex6*|*Screenshot of A1 Ex7*|
| :---                 | :---                 |
|![A1 Exercise 6](img/a1exercise6.png)|![A1 Exercise 7](img/a1exercise7.png)|




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bpk16/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

