


# LIS3781 Advanced Database Management

## Brian Kelly

### Assignment #2 Requirements:

1. Screenshot of MySQL Code
2. Screenshot of Populated Tables

#### Assignment Screenshots:

|*Screenshot of MySQL Code (Company Table)*|*Screenshot of MySQL Code (Customer Table)*|
| :---                                     | :---                                      |
|![MySQL Code (Company Table)](img/a2SQLCodeA.png)|![MySQL Code (Customer Table)](img/a2SQLCodeB.png)|

|*Screenshot of Populated Tables*|
| :---                           |
|![Populated Tables](img/a2TablePic.png)|
