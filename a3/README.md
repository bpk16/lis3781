


# LIS4331 Advanced Mobile Applications Development

## Brian Kelly

### Assignment #3 Requirements:

1. Screenshot of my SQL Code (Part A-E)
2. Screenshot of my populated tables in Oracle
3. Screenshot of SQL code for required reports (optional)


#### Assignment Screenshots:

|*Screenshot of SQL Code(Part A)*|*Screenshot of SQL Code(Part B)*|
| :---                           | :---                           |
|![SQL Code (Part A)](img/sqlCodePartA.png)|![SQL Code (Part B)](img/sqlCodePartB.png)|

|*Screenshot of SQL Code(Part C)*|*Screenshot of SQL Code(Part D)*|
| :---                           | :---                           |
|![SQL Code (Part C)](img/sqlCodePartC.png)|![SQL Code (Part D)](img/sqlCodePartD.png)|

|*Screenshot of SQL Code(Part E)*|*Screenshot of Oracle - Populated Tables*|
| :---                           | :---                                    |
|![SQL Code (Part E)](img/sqlCodePartE.png)|![Oracle - Populated Tables](img/oraclePopulatedTables.png)|

|*Gif of SQL Reports*|
| :---               |
|![SQL Reports](img/sqlreports1-17.gif)|




