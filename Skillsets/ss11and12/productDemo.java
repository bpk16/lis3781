import java.util.Scanner;
import java.text.DecimalFormat;

class productDemo
{
    public static void main(String[] args) 
    {
        String cd = "";
        String des = "";
        double pc = 0.0;
        DecimalFormat currency = new DecimalFormat("$##.##");

        Scanner sc = new Scanner(System.in);

        System.out.println("*Below are default constructor values*");

        productClass p1 = new productClass();
        System.out.println("Code: " + p1.getCode());
        System.out.println("Description: " + p1.getDescription());
        System.out.println("Price: " + currency.format(p1.getPrice()));

        System.out.println("*Below are user entered constructor values*");
        
        System.out.print("Code: ");
        cd = sc.nextLine();
        
        System.out.print("Description: ");
        des = sc.nextLine();
        
        System.out.print("Price: ");
        pc = sc.nextDouble();

        productClass p2 = new productClass(cd, des, pc);
        System.out.println("Code: " + p2.getCode());
        System.out.println("Description: " + p2.getDescription());
        System.out.println("Price: " + currency.format(p2.getPrice()));

        System.out.println("*Below are values using setter methods to pass literal values, then the print() method to display the values*");
        p2.setCode("xyz789\n");
        p2.setDescription("Test Widget\n");
        p2.setPrice(89.99);
        p2.print();
    }
}
