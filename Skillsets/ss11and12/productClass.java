import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;
import java.text.DecimalFormat;

public class productClass 
{
    private String code;
    private String description;
    private double price;

    public  productClass()
    {
        System.out.println("\nInside product default constructor.");
        code = "abc123";
        description = "My Widget";
        price = 49.99;
        
    }

    public  productClass(String code, String description, double price)
    {
        System.out.println("\nInside product  constructor with parameters.");
        this.code = code;
        this.description = description;
        this.price = price;
    }

    public String getCode()
    {
        return code;
    }
    public void setCode(String cd)
    {
        code = cd;
    }
    public String getDescription()
    {
        return description;
    }
    public void setDescription(String des)
    {
        description = des;
    }
    public double getPrice()
    {
        return price;
    }
    public void setPrice(double pc)
    {
        price = pc;
    }
    public void print()
    {
        DecimalFormat currency = new DecimalFormat("$##.##");
        System.out.println("Code: " + code + "Description: " + description + "Price: " + currency.format(price));
    }
}