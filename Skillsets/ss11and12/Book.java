class Book extends productClass
{
    private String author;

    public Book()
    {
        super();
        System.out.println("*Inside book default constructor*");
        author = "John Doe";
    }
    public Book(String cd, String des, double pc, String au)
    {
        super(cd, des, pc);
        System.out.println("Inside book constructor with parameters");

        author = au;
    }
    public String getAuthor()
    {
        return author;
    }
    public void setAuthor(String au)
    {
        author = au;
    }
    public void print()
    {
        System.out.println("Author: " + author);
    }
}
