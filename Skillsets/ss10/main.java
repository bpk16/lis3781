import java.util.Scanner;

class main
{
    public static void main(String[]args)
    {
        double miles = 0.0;
        double mph = 0.0;
        double validMiles = 0.0;
        double validMPH = 0.0;
        String choice = "";

        Scanner sc = new Scanner(System.in);

        methods.getRequirements();

        do
        {
            miles = methods.validateMilesDataType();
            validMiles =methods.validateMilesRange(miles);

            mph = methods.validateMPHDataType();
            validMPH =methods.validateMPHRange(miles);

            methods.calculateTravelTime(validMiles, validMPH);

            System.out.print("\nContinue? (y/n): ");
            choice = sc.next();
            System.out.println();
        }
    
        while (choice.equalsIgnoreCase("y"));
    }
}