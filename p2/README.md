
# LIS3781 Advanced Database Management

## Brian Kelly

### Project #2 Requirements:

1. Screenshot of at least one mongoDB shell command(s)
2. At least one required report
3. Answer Chapter 16 questions

#### Assignment Requirements:

|*MongoDB Shell Command*|
| :---             |
|![MongoDB Shell Command](img/ShellCommand.png)|

|*MongoDB Report*|
| :---             |
|![MongoDB Report](img/report.png)|